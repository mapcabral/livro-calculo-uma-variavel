set terminal table; set output "figurasPGF/t7.table"; set format "%.5f"
set samples 25; plot [x=0.12:3.45] (x*x*x*x-16)/(x*(x*x-16))
