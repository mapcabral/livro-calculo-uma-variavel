set terminal table; set output "figurasPGF/sinxxx3.table"; set format "%.5f"
set samples 130; plot [x=0.01:0.1] 2*x+3*abs(x)**1.4*sin(1/x)+0.1
