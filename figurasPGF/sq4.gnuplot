set terminal table; set output "figurasPGF/sq4.table"; set format "%.5f"
set samples 25; plot [x=-2:2] 1-x*x
