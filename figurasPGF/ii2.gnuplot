set terminal table; set output "figurasPGF/ii2.table"; set format "%.5f"
set samples 25; plot [x=-5:6] (2*x*x-20)/10
