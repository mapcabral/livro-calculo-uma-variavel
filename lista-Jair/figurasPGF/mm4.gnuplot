set terminal table; set output "figurasPGF/mm4.table"; set format "%.5f"
set samples 100; plot [x=0.2:6] (1-x)/x/x
