set terminal table; set output "figurasPGF/ll6.table"; set format "%.5f"
set samples 25; plot [x=2.2:7] (2*x**2-2*x)/((x-2)*(x+1))
