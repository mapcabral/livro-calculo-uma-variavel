set terminal table; set output "figurasPGF/iw5.table"; set format "%.5f"
set samples 25; plot [x=-3.1415:3.1415] 2*(sin(x) - sin(2*x)/2 )
