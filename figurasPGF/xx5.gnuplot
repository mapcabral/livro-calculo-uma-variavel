set terminal table; set output "figurasPGF/xx5.table"; set format "%.5f"
set samples 25; plot [x=1.14:2.14] 0.5/abs(x-1)
