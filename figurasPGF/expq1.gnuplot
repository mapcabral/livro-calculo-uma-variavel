set terminal table; set output "figurasPGF/expq1.table"; set format "%.5f"
set samples 25; plot [x=-2:0] 1.5*exp(1/x/5)
