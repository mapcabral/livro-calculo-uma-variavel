set terminal table; set output "figurasPGF/jj4.table"; set format "%.5f"
set samples 25; plot [x=2:3] (3-x)*(x-2)*(x-2)*(x-5)*6
