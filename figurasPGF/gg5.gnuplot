set terminal table; set output "figurasPGF/gg5.table"; set format "%.5f"
set samples 25; plot [x=-3:3] -sqrt(9-x*x)
