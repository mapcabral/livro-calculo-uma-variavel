set terminal table; set output "figurasPGF/sq3.table"; set format "%.5f"
set samples 25; plot [x=-1:0] abs(x)**(1/3.0)
