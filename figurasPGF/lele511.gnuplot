set terminal table; set output "figurasPGF/lele511.table"; set format "%.5f"
set samples 25; plot [x=-0.5:0] 2*sqrt(abs(x))*(x-1)
