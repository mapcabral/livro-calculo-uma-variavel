set terminal table; set output "figurasPGF/lele512.table"; set format "%.5f"
set samples 25; plot [x=0:1.4] 2*sqrt(abs(x))*(x-1)
