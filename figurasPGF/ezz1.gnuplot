set terminal table; set output "figurasPGF/ezz1.table"; set format "%.5f"
set samples 25; plot [x=-3:-0.2] exp(x)/x
