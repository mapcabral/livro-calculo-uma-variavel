set terminal table; set output "figurasPGF/cc21.table"; set format "%.5f"
set samples 25; plot [x=-1:4] (x-2)**2/4
