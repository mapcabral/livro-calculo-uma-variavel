set terminal table; set output "figurasPGF/m6.table"; set format "%.5f"
set samples 25; plot [x=-0.4:-0.1] x*sin(1/x)
