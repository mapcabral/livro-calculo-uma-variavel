ERRATAS da V1.1 de agosto de 2010 da Lista do Jair

ENUNCIADOS

p. 2, exercício 2: Determine a e b de modo que o limite seja 0 (zero).

RESPOSTAS

p. 18, 1(b) 7y+3x-2=0 (faltou o =0)

p. 18, exercicio 2: "Colocando o mesmo denominador obtemos

 (2-a)x^2  + (b-a)x + 3  (estava b-1 ao invés de b-a)

Como o limite é 0 (estava 1), o coeficiente que multiplica x
deve ser igual a zero: b-a=0. Logo a=b=2.

p. 25, exercicio 4 (d): -1/4 cos^4(theta)  (faltava o elevado a 4 no
cosseno)

p.26 exercicio 2 (b) 2y^2-4=y^2 (estava 2y^2 + 4)

p. 27, exercício 6: pi (2e^2-1)/(2e^2). O denominador estava errado
(2e).


