set terminal table; set output "figurasPGF/ezz3.table"; set format "%.5f"
set samples 25; plot [x=-0.95:0.95] log(1-x**2)+1
