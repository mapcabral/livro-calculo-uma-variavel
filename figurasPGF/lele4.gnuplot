set terminal table; set output "figurasPGF/lele4.table"; set format "%.5f"
set samples 25; plot [x=-6:-2.17] -x**3/(x+2)/(x-1)**2 + 2
