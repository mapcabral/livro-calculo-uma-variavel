set terminal table; set output "figurasPGF/gg2.table"; set format "%.5f"
set samples 25; plot [x=1:4] sqrt(x-1)
