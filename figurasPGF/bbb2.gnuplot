set terminal table; set output "figurasPGF/bbb2.table"; set format "%.5f"
set samples 25; plot [x=-2:2] (x*x-4)/2
