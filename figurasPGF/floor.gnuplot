set terminal table; set output "figurasPGF/floor.table"; set format "%.5f"
set samples 100; plot [x=-4:4] floor(x)
