set terminal table; set output "figurasPGF/vb2.table"; set format "%.5f"
set samples 25; plot [x=-3:3] abs(abs(x)-1)
