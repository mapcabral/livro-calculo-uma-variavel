set terminal table; set output "figurasPGF/jj2.table"; set format "%.5f"
set samples 25; plot [x=-1.7:3.4] (x-2)*(x-2)*(x+1)/3
