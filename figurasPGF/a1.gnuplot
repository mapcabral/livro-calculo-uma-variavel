set terminal table; set output "figurasPGF/a1.table"; set format "%.5f"
set samples 25; plot [x=-1:1.6] (x*x-1)
