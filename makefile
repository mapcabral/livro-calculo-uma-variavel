# Makefile para automatizar a compilacao de meus documentos TeX/LaTeX
DATE=`date +%Y-%b-%d-%a-%Hh%Mm-``hostname`

# Colocar aqui o nome (base) dos arquivos 
TITLE = cursoCalculoI-livro
TITLEexe = cursoCalculoI-exercicios
FILES = curso-analise-real.tex capa.tex sobreautores.tex prefacio.tex \
        cap1.tex cap2.tex cap3.tex cap4.tex  cap5.tex \
        cap6.tex cap7.tex cap8.tex cap9.tex cap10.tex\
        cap1-exercicios.tex cap2-exercicios.tex cap3-exercicios.tex \
	cap4-exercicios.tex cap5-exercicios.tex \
        cap6-exercicios.tex cap7-exercicios.tex cap8-exercicios.tex \
	cap9-exercicios.tex cap10-exercicios.tex\
	biblio.tex
# o shell escape é para o pgf poder chamar o gnuplot atraves do lates.
LATEX = pdflatex --shell-escape

##############################################################
# GUIA
##############################################################
# default: gerar ps normal
# all: gera a4-pdf, a5-pdf, djvu (ps-a4, ps-a5-book) 
#
#
#

default: all

# Gerando livro com capa para editora do IM
editora-im: 
	pdftk PreCapa-Livro-Calculo-Editora-IM.pdf cursoCalculoI-livro.pdf  cat output curso-calculoI-editora-IM.pdf
	ls -lh curso-calculoI-editora-IM.pdf
#       pdftk mantém o índice do livro

all: miolo editora-im


miolo:
	${LATEX}  ${TITLE}.tex 
	makeindex ${TITLE}.idx
	${LATEX}  ${TITLE}.tex 

#############
# Exercicios somente
#############
exercicios:
	${LATEX} ${TITLEexe}.tex 
	ls -lh ${TITLEexe}.pdf

##############################################################
# CLEAN
##############################################################
clean:
	rm -f ${TITLE}.aux ${TITLE}.bbl ${TITLE}.blg ${TITLE}.fot ${TITLE}.idx ${TITLE}.ilg ${TITLE}.ind ${TITLE}.log ${TITLE}.lof ${TITLE}.toc ${TITLE}.out

cleanall: clean
	rm -f ${TITLE}.dvi ${TITLE}.ps ${TITLE}.pdf
	rm -i ${TITLE}*.ps ${TITLE}*.pdf ${TITLE}*.djvu 

##############################################################
# SPELL
##############################################################
spell: spell0 spell1 spell2 spell3 spell4 spell5 spell6
spell6:
	aspell -l PT_BR  check aplicacoes-de-integral-texto.tex
	aspell -l PT_BR  check aplicacoes-de-integral-exercicios.tex
spell5:
	aspell -l PT_BR  check integral-texto.tex
	aspell -l PT_BR  check integral-exercicios.tex
spell4:
	aspell -l PT_BR  check aplicacoes-de-derivada-texto.tex
	aspell -l PT_BR  check aplicacoes-de-derivada-exercicios.tex
spell3:
	aspell -l PT_BR  check derivada-texto.tex
	aspell -l PT_BR  check derivada-exercicios.tex
spell2:
	aspell -l PT_BR  check continuidade-texto.tex
	aspell -l PT_BR  check continuidade-exercicios.tex
spell1:
	aspell -l PT_BR  check limite-texto.tex
	aspell -l PT_BR  check limite-exercicios.tex
spell0:	
	aspell -l PT_BR  check FRONTMATTER-prefacio.tex
	aspell -l PT_BR  check FRONTMATTER-sobreautores.tex
	aspell -l PT_BR  check FRONTMATTER-capa.tex
	aspell -l PT_BR  check FRONTMATTER-agradecimentos.tex

##############################################################
# FULL
##############################################################
full:
	grep Overfull *log| grep "[0-9][0-9][0-9]\."
	grep Overfull *log| grep "[2-9][0-9]\."


