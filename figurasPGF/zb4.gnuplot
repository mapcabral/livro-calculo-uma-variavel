set terminal table; set output "figurasPGF/zb4.table"; set format "%.5f"
set samples 25; plot [x=-1:3] sqrt(x+1)
