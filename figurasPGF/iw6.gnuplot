set terminal table; set output "figurasPGF/iw6.table"; set format "%.5f"
set samples 25; plot [x=-3.1415:3.1415] 2*(sin(x) - sin(2*x)/2 + sin(3*x)/3 - sin(4*x)/4 + sin(5*x)/5 - sin(6*x)/6 )
