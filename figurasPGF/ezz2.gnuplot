set terminal table; set output "figurasPGF/ezz2.table"; set format "%.5f"
set samples 25; plot [x=0.2:3] exp(x)/x
