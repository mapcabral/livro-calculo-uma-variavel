set terminal table; set output "figurasPGF/xsin1overx.table"; set format "%.5f"
set samples 25; plot [x=-0.4:-0.1] sin(1/x)
