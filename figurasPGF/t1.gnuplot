set terminal table; set output "figurasPGF/t1.table"; set format "%.5f"
set samples 25; plot [x=-10:-2.2] (x*x+4*x)/(x*x-4)
