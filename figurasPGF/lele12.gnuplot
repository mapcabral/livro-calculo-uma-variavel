set terminal table; set output "figurasPGF/lele12.table"; set format "%.5f"
set samples 25; plot [x=-1.57:1.57] (x-1)**2*(x+1)**2
