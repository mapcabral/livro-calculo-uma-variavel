set terminal table; set output "figurasPGF/xsin1overx3.table"; set format "%.5f"
set samples 160; plot [x=-0.1:0.1] sin(1/x)
