set terminal table; set output "figurasPGF/lele3.table"; set format "%.5f"
set samples 25; plot [x=-12:12] 1.2*x + sin(x)
