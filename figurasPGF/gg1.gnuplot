set terminal table; set output "figurasPGF/gg1.table"; set format "%.5f"
set samples 25; plot [x=0.1:1] log(x)+1
