set terminal table; set output "figurasPGF/ll3.table"; set format "%.5f"
set samples 25; plot [x=-7:4] 4*exp(x+2)/(1+exp(x+2))-1
