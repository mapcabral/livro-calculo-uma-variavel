set terminal table; set output "figurasPGF/qq5.table"; set format "%.5f"
set samples 25; plot [x=-2.0:-1.8] sqrt(2*2 - x*x)
