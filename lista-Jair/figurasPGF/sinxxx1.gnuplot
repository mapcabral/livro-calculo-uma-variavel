set terminal table; set output "figurasPGF/sinxxx1.table"; set format "%.5f"
set samples 130; plot [x=0.01:0.1] x+ x*sin(1/x)+0.1
