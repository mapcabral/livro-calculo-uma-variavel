set terminal table; set output "figurasPGF/xzq7.table"; set format "%.5f"
set samples 25; plot [x=-2:2] exp(-x*x)
