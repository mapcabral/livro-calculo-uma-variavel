set terminal table; set output "figurasPGF/x4.table"; set format "%.5f"
set samples 25; plot [x=0.01:10] log(x)
