set terminal table; set output "figurasPGF/t10.table"; set format "%.5f"
set samples 25; plot [x=-10:-4.6] (x*x*x*x-16)/(x*(x*x-16))+8
