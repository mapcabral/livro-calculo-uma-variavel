set terminal table; set output "figurasPGF/uv3.table"; set format "%.5f"
set samples 25; plot [x=2.25:8] (x*x-1)/x/(x-2)
