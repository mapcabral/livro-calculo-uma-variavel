set terminal table; set output "figurasPGF/osc.table"; set format "%.5f"
set samples 50; plot [x=1:4] (abs(x-1)**(0.5))*sin(20*x)/1.0+2
