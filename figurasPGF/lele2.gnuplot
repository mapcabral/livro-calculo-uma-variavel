set terminal table; set output "figurasPGF/lele2.table"; set format "%.5f"
set samples 25; plot [x=-3.7:2.5] (2*x**3 + 3*x**2 - 12*x + 1)/8
