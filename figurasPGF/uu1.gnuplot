set terminal table; set output "figurasPGF/uu1.table"; set format "%.5f"
set samples 25; plot [x=-3:-1.15] 1/(x*x-1)
