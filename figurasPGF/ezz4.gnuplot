set terminal table; set output "figurasPGF/ezz4.table"; set format "%.5f"
set samples 25; plot [x=-2:5] exp((2-x)*(x-1))+1
