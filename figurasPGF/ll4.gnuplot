set terminal table; set output "figurasPGF/ll4.table"; set format "%.5f"
set samples 25; plot [x=-7:-1.2] (2*x**2-2*x)/((x-2)*(x+1))
