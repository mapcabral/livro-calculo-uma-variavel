set terminal table; set output "figurasPGF/uu2.table"; set format "%.5f"
set samples 25; plot [x=-0.85:0.85] 1/(x*x-1)
