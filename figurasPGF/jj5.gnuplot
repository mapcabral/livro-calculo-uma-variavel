set terminal table; set output "figurasPGF/jj5.table"; set format "%.5f"
set samples 25; plot [x=3:5.3] (3-x)*(x-2)*(x-2)*(x-5)/2
