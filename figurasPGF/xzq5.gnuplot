set terminal table; set output "figurasPGF/xzq5.table"; set format "%.5f"
set samples 25; plot [x=-2:2] 1/(x*x+1)
