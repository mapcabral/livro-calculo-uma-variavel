set terminal table; set output "figurasPGF/bab3.table"; set format "%.5f"
set samples 25; plot [x=0.1:0.4] sin(1/x)
