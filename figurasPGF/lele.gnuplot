set terminal table; set output "figurasPGF/lele.table"; set format "%.5f"
set samples 25; plot [x=-3:3] (x*x-9)*(x*x-1)/6
