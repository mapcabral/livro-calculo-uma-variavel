set terminal table; set output "figurasPGF/ll7.table"; set format "%.5f"
set samples 25; plot [x=-5:-1.2] (1+x*x)/(1-x*x)
