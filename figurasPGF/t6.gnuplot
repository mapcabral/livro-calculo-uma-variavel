set terminal table; set output "figurasPGF/t6.table"; set format "%.5f"
set samples 25; plot [x=4.4:10] (2*x*x-8)/(16-x*x)
