set terminal table; set output "figurasPGF/zb5.table"; set format "%.5f"
set samples 25; plot [x=-2:-1] x*x-2
