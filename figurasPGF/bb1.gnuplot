set terminal table; set output "figurasPGF/bb1.table"; set format "%.5f"
set samples 25; plot [x=0:2] 1+sqrt(x)
