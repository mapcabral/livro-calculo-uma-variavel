set terminal table; set output "figurasPGF/zeze04.table"; set format "%.5f"
set samples 25; plot [x=-4:4] x*x/(x*x+3)
