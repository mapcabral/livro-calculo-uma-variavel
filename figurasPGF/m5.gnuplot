set terminal table; set output "figurasPGF/m5.table"; set format "%.5f"
set samples 25; plot [x=1:2.5] x*sin(1/x)
