set terminal table; set output "figurasPGF/t4.table"; set format "%.5f"
set samples 25; plot [x=-10:-4.4] (2*x*x-8)/(16-x*x)
