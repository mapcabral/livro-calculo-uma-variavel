set terminal table; set output "figurasPGF/xx0.table"; set format "%.5f"
set samples 140; plot [x=0:8] (sqrt(abs(x-1))+0.5)*2*sin(10*x)-1
