set terminal table; set output "figurasPGF/ezz5.table"; set format "%.5f"
set samples 25; plot [x=-7:0.98] x**3*exp(x)
