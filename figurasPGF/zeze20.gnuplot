set terminal table; set output "figurasPGF/zeze20.table"; set format "%.5f"
set samples 25; plot [x=-1.5:3.3] (x**3-3*x**2)/3+2
