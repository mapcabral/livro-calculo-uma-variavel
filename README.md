Curso de Cálculo de Uma Variável (livro)

ISBN: 978-65-86502-05-3

Pode-se gerar o livro no formato PDF (a4) com make.

DESCRIÇÃO

Um livro de Cálculo I (cálculo diferencial e integral de uma variável) para curso de graduação contendo muitos 
exercícios em cada capítulo.
Utilizado na UFRJ (Universidade Federal do Rio de Janeiro) e em várias instituições pelo Brasil.


AUTOR

Marco Aurélio Palumbo Cabral (Professor do Instituto de Matemática da UFRJ)

SÍTIOS

Publicado em versão digital em abril de 2022 pela Editora do Instituto de Matemática da UFRJ. Acesse
aqui o PDF do livro: http://www.im.ufrj.br/index.php/pt/estrutura/e-books-im

Visite https://sites.google.com/matematica.ufrj.br/mapcabral/livros-e-videos para ter acesso a outros materiais relacionados a este livro.

HISTÓRIA DAS VERSÕES

Primeira Edição digital de abril de 2022 pela Editora do Instituto de Matemática da UFRJ: http://www.im.ufrj.br/index.php/pt/estrutura/e-books-im

Versão 3.0 (01/agosto/2013); 3a Edição; 250 páginas. Lista de Exercícios do Prof. Jair. sem modificações (V1.3 Fevereiro de 2011)
    Inclusão de índice e links clicáveis no PDF.
    Melhorado o sistema de numeração dos exercícios e incluído índice remissivo. 
    Foi reescrita a Seção Funções Transcendentes e Raiz. 
    Colocamos a Seção de Derivação Implícita no Capítulo de Derivada. 
    Foram incluídos exercícios de integração por cascas cilíndricas.
    Corrigimos diversos erros detectados no texto com auxílio de professores e alunos, entre eles os alunos
    alunos José Guilherme T.  Monteiro (Engenharia de Controle e Automação UFRJ turma 2011) e Joshua Silveira Kritz
    (Matemática Aplicada UFRJ turma 2013).

Versão 2.1 2011 (outubro); 2a Edição; 257 páginas. Lista de Exercícios do Prof. Jair. sem modificações (V1.3 Fevereiro de 2011)
    Acrescentamos seções de integração por substituição trigonométrica e da teoria da decomposição por frações parciais.
    Tratamos de Integração Trigonométrica através de um Teorema, ao invés do modo usual, através de truques. 
    Reescrevemos a Seção de Integração de Funções Racionais.  Acrescentamos muitos exercícios de Desafio. 
    Corrigimos diversos erros detectados no texto com auxílio de professores e alunos, entre eles o aluno 
    José Guilherme T. Monteiro, do curso da UFRJ de Engenharia de Controle e Automação da turma 2011.

Versão 1.5 2010 (julho);   1a Edição; 229 páginas. Respostas dos exercícios. Lista de Exercícios do Prof. Jair. V1.3 Fevereiro de 2011

Versão 1.0 2008 (dezembro); Versão inicial do livro com 60 páginas.

LICENÇA

Creative Commons CC BY-NC-SA 3.0 BR, Atribuição (BY) Uso Não-Comercial (NC) CompartilhaIgual (SA) 3.0 Brasil.
Veja arquivo LICENSE para detalhes.

