set terminal table; set output "figurasPGF/lele9.table"; set format "%.5f"
set samples 25; plot [x=-0.4:4.7] x**3/3-(5*x**2)/2+6*x
