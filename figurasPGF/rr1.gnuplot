set terminal table; set output "figurasPGF/rr1.table"; set format "%.5f"
set samples 100; plot [x=-3:3] x-floor(x)
