set terminal table; set output "figurasPGF/bbb1.table"; set format "%.5f"
set samples 25; plot [x=-4:4.5] abs(x*x-4)/2
