set terminal table; set output "figurasPGF/m1.table"; set format "%.5f"
set samples 25; plot [x=-2.5:-1] x*sin(1/x)
