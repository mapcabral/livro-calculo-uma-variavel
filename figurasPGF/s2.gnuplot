set terminal table; set output "figurasPGF/s2.table"; set format "%.5f"
set samples 25; plot [x=-1.4:1.4] 1/cos(x)+1
