set terminal table; set output "figurasPGF/za2.table"; set format "%.5f"
set samples 25; plot [x=0:1] sqrt(x)
