set terminal table; set output "figurasPGF/lk1.table"; set format "%.5f"
set samples 25; plot [x=-7:0.45] 4*x/(x-1)**2
