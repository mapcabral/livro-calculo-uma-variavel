set terminal table; set output "figurasPGF/t5.table"; set format "%.5f"
set samples 25; plot [x=-3.66:3.66] (2*x*x-8)/(16-x*x)
