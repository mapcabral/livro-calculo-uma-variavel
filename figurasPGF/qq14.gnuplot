set terminal table; set output "figurasPGF/qq14.table"; set format "%.5f"
set samples 25; plot [x=-1.2:2.7] x*x*x-3*x*x+2*x
