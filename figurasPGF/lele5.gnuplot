set terminal table; set output "figurasPGF/lele5.table"; set format "%.5f"
set samples 25; plot [x=-1.81:0.82] -x**3/(x+2)/(x-1)**2 + 2
