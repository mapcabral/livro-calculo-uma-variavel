set table "figurasPGF/xxb.table"; set format "%.5f"
set samples 170.0; plot [x=-8:8] (3*sin(9*x)/(abs(x+1)**(1.7)+1)) -x/2+0.5
