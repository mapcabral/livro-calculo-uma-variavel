set terminal table; set output "figurasPGF/ll2.table"; set format "%.5f"
set samples 25; plot [x=-2.9:2.9] x**4/4 - 2*x**2+4.5
