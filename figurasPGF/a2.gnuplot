set terminal table; set output "figurasPGF/a2.table"; set format "%.5f"
set samples 25; plot [x=0:1.6] abs(x*x-1)
