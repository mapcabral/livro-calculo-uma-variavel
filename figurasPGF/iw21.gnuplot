set terminal table; set output "figurasPGF/iw21.table"; set format "%.5f"
set samples 25; plot [x=-1.5:1.5] 1-x*x
