set terminal table; set output "figurasPGF/qq4.table"; set format "%.5f"
set samples 25; plot [x=-1.8:1.8] sqrt(2*2 - x*x)
