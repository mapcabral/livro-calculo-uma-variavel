set terminal table; set output "figurasPGF/uu4.table"; set format "%.5f"
set samples 25; plot [x=-6:6] x/(x*x+1)
