set terminal table; set output "figurasPGF/cc20.table"; set format "%.5f"
set samples 25; plot [x=-0.2:1.15] exp(x*x)
