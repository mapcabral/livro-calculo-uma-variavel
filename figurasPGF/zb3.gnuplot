set terminal table; set output "figurasPGF/zb3.table"; set format "%.5f"
set samples 25; plot [x=-2:2] x*x-2
