set terminal table; set output "figurasPGF/mm1.table"; set format "%.5f"
set samples 25; plot [x=-0.5:4] x*exp(-x)
