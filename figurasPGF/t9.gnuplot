set terminal table; set output "figurasPGF/t9.table"; set format "%.5f"
set samples 25; plot [x=4.6:10] (x*x*x*x-16)/(x*(x*x-16))-8
