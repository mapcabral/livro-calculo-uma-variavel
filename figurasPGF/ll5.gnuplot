set terminal table; set output "figurasPGF/ll5.table"; set format "%.5f"
set samples 25; plot [x=-0.7:1.7] 2*(2*x**2-2*x)/((x-2)*(x+1))+ 0.7
