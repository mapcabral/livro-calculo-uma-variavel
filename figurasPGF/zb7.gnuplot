set terminal table; set output "figurasPGF/zb7.table"; set format "%.5f"
set samples 25; plot [x=1.05:3] log(x-1)
