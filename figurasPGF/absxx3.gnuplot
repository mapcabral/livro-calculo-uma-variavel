set terminal table; set output "figurasPGF/absxx3.table"; set format "%.5f"
set samples 25; plot [x=-3.5:3.5] (x*x-4)/2
