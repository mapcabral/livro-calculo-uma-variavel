set terminal table; set output "figurasPGF/ll9.table"; set format "%.5f"
set samples 25; plot [x=1.2:4] (1+x*x)/(1-x*x)
