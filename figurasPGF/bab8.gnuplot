set terminal table; set output "figurasPGF/bab8.table"; set format "%.5f"
set samples 25; plot [x=-0.2:-0.1] sin(1/x)
