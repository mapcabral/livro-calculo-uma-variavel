set terminal table; set output "figurasPGF/sqrtabs2.table"; set format "%.5f"
set samples 25; plot [x=-2:0] -sqrt(-x)
