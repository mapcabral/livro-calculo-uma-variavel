set terminal table; set output "figurasPGF/jj6.table"; set format "%.5f"
set samples 25; plot [x=-3:3] (x*x*x-8)/2
