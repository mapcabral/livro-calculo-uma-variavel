set terminal table; set output "figurasPGF/qq2.table"; set format "%.5f"
set samples 25; plot [x=0:1.5] -sqrt(x)
