set terminal table; set output "figurasPGF/t8.table"; set format "%.5f"
set samples 25; plot [x=-3.45:-0.12] (x*x*x*x-16)/(x*(x*x-16))
