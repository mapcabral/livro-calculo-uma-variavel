set terminal table; set output "figurasPGF/osc2.table"; set format "%.5f"
set samples 600; plot [x=-4:0.9] sin(70/(1-x)**(3.3))/1.5-2
