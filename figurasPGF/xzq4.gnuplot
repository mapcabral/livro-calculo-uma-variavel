set terminal table; set output "figurasPGF/xzq4.table"; set format "%.5f"
set samples 25; plot [x=0:0.6] cos(0.3) - sin(0.3)*(x-0.3)
