set terminal table; set output "figurasPGF/uv2.table"; set format "%.5f"
set samples 25; plot [x=0.1:1.75] (x*x-1)/x/(x-2)
