set terminal table; set output "figurasPGF/ii6.table"; set format "%.5f"
set samples 25; plot [x=-2:2.5] x*x
