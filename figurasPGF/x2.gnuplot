set terminal table; set output "figurasPGF/x2.table"; set format "%.5f"
set samples 25; plot [x=0.01:0.85] 1/log(x)
