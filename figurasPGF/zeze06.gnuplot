set terminal table; set output "figurasPGF/zeze06.table"; set format "%.5f"
set samples 25; plot [x=-3:3] x*exp(1-x*x)
