set terminal table; set output "figurasPGF/q2.table"; set format "%.5f"
set samples 25; plot [x=-4:0] 1.5*exp(1/x/5)
