set terminal table; set output "figurasPGF/xxe2.table"; set format "%.5f"
set samples 25; plot [x=-4:0] x*(x+2)/1.7
