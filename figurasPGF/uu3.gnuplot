set terminal table; set output "figurasPGF/uu3.table"; set format "%.5f"
set samples 25; plot [x=1.15:3] 1/(x*x-1)
