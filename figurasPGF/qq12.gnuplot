set terminal table; set output "figurasPGF/qq12.table"; set format "%.5f"
set samples 25; plot [x=-1.5:1.5] x*x*x -x
