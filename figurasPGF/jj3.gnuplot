set terminal table; set output "figurasPGF/jj3.table"; set format "%.5f"
set samples 25; plot [x=1:2] (3-x)*(x-2)*(x-2)*(x-5)/2
