set terminal table; set output "figurasPGF/mm2.table"; set format "%.5f"
set samples 25; plot [x=-0.7:8] x*x*exp(-x)
