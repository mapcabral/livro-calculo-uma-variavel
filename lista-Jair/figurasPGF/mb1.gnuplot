set terminal table; set output "figurasPGF/mb1.table"; set format "%.5f"
set samples 25; plot [x=-4:-0.2] (1-x)/x/x
