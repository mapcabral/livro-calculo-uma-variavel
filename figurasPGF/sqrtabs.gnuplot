set terminal table; set output "figurasPGF/sqrtabs.table"; set format "%.5f"
set samples 25; plot [x=0:2] -sqrt(x)
