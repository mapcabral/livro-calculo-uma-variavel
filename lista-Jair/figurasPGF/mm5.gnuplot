set terminal table; set output "figurasPGF/mm5.table"; set format "%.5f"
set samples 25; plot [x=0.01:8] x*x*exp(-x)
