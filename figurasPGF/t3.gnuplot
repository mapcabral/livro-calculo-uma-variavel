set terminal table; set output "figurasPGF/t3.table"; set format "%.5f"
set samples 25; plot [x=2.5:10] (x*x+4*x)/(x*x-4)
