set terminal table; set output "figurasPGF/qq6.table"; set format "%.5f"
set samples 25; plot [x=1.8:2.0] sqrt(2*2 - x*x)
