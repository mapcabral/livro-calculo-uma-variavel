set terminal table; set output "figurasPGF/sinxxx2.table"; set format "%.5f"
set samples 100; plot [x=-0.01:0.01] 2*x+3*abs(x)**1.4*sin(1/x)+0.1
