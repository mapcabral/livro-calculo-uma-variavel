set terminal table; set output "figurasPGF/lele6.table"; set format "%.5f"
set samples 25; plot [x=1.36:6] -x**3/(x+2)/(x-1)**2 + 2
