set terminal table; set output "figurasPGF/lk2.table"; set format "%.5f"
set samples 25; plot [x=1.5:5.5] x/(x-1)**2
