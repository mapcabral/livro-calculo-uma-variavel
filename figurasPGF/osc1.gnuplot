set terminal table; set output "figurasPGF/osc1.table"; set format "%.5f"
set samples 600; plot [x=-4:0.9] sin(50/(1-x)**(0.5))/0.5+1
