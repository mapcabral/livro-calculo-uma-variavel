set terminal table; set output "figurasPGF/zeze08.table"; set format "%.5f"
set samples 25; plot [x=-6:1] x*x*exp(x)
