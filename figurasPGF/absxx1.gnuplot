set terminal table; set output "figurasPGF/absxx1.table"; set format "%.5f"
set samples 25; plot [x=-3.5:3.5] abs(x*x-4)/2
