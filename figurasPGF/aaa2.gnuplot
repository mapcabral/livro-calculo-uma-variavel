set terminal table; set output "figurasPGF/aaa2.table"; set format "%.5f"
set samples 25; plot [x=-0.9:2.1] x*(2-x)*(x+2)/1.5 +1
