set terminal table; set output "figurasPGF/xzq2.table"; set format "%.5f"
set samples 25; plot [x=-1:2] cos(0.3) - sin(0.3)*(x-0.3)
