set terminal table; set output "figurasPGF/x3.table"; set format "%.5f"
set samples 25; plot [x=1.15:10] 1/log(x)
