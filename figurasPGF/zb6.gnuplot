set terminal table; set output "figurasPGF/zb6.table"; set format "%.5f"
set samples 25; plot [x=-1:1] sqrt(x+1)
