set terminal table; set output "figurasPGF/zeze07.table"; set format "%.5f"
set samples 25; plot [x=0.01:2] x*log(x)
