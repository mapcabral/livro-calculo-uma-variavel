set terminal table; set output "figurasPGF/ll8.table"; set format "%.5f"
set samples 25; plot [x=-0.8:0.8] (1+x*x)/(1-x*x)
