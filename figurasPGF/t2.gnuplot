set terminal table; set output "figurasPGF/t2.table"; set format "%.5f"
set samples 25; plot [x=-1.85:1.5] (x*x+4*x)/(x*x-4)
