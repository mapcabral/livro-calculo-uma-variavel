set terminal table; set output "figurasPGF/qq13.table"; set format "%.5f"
set samples 25; plot [x=-2.4:2.4] sin(3.14*x)
