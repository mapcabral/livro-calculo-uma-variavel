set terminal table; set output "figurasPGF/jj1.table"; set format "%.5f"
set samples 25; plot [x=-3.2:3] (x-2)*(x+3)*(1-x)/3
