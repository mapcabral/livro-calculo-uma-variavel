set terminal table; set output "figurasPGF/ii4.table"; set format "%.5f"
set samples 25; plot [x=0:6] -sqrt(x)
