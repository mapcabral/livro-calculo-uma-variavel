set terminal table; set output "figurasPGF/xx6.table"; set format "%.5f"
set samples 25; plot [x=-0.14:0.86] 0.5/abs(x-1)
